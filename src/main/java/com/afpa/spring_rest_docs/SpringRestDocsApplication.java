package com.afpa.spring_rest_docs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.afpa.spring_rest_docs.*"})
public class SpringRestDocsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestDocsApplication.class, args);
	}

}
