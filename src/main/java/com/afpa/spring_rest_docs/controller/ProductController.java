package com.afpa.spring_rest_docs.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.spring_rest_docs.entity.Product;
import com.afpa.spring_rest_docs.service.ProductService;

@RestController
public class ProductController 
{
	
	@Autowired
	private ProductService productService;
	
	/**
	 * Get all existing products
	 * @return List of Products
	 */
	@GetMapping("/products")
	public List<Product> findAllProduct()
	{
		return productService.getAllProducts();
	}
	
	/**
	 * Get a product by id
	 * @param id product identifier
	 * @return a Product
	 */
	@GetMapping("/products/{id}")
	public Optional<Product> findProductById(@PathVariable int id)
	{
		return productService.getProductById(id);
	}
	
}
