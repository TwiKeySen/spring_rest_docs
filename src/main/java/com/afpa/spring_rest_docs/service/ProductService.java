package com.afpa.spring_rest_docs.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.spring_rest_docs.entity.Product;
import com.afpa.spring_rest_docs.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public List<Product> getAllProducts() 
	{
		return productRepository.findAll();
	}

	
	public Optional<Product> getProductById(int id) 
	{
		return productRepository.findById(id);
	}
}
