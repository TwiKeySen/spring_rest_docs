package com.afpa.spring_rest_docs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.afpa.spring_rest_docs.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> 
{
}